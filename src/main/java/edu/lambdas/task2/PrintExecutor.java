package edu.lambdas.task2;

import java.util.ArrayList;
import java.util.List;

public class PrintExecutor {

    private final List<PrintStringCommand> printOperations = new ArrayList<>();

    public String executeOperation(PrintStringCommand printCommand) {
        printOperations.add(printCommand);
        return printCommand.execute();
    }
}
