package edu.lambdas.task2;

@FunctionalInterface
public interface PrintStringCommand {
    String execute();
}
