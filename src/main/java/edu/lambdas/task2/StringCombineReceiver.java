package edu.lambdas.task2;

public class StringCombineReceiver {

    private String mutablePartOfString;

    public StringCombineReceiver(String mutablePartOfString) {
        this.mutablePartOfString = mutablePartOfString;
    }

    public String buildMessageWithReferenceText() {
        return "This string was edited with method reference " + mutablePartOfString;
    }

    public String buildMessageWithLambdaText() {
        return "This string was edited with lambda expression " + mutablePartOfString;
    }
}
