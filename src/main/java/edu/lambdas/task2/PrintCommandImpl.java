package edu.lambdas.task2;

public class PrintCommandImpl implements PrintStringCommand {

    private String mutablePartOfString;

    public PrintCommandImpl(String mutablePartOfString) {
        this.mutablePartOfString = mutablePartOfString;
    }

    @Override
    public String execute() {
        return "This string was edited by command class approach " + mutablePartOfString;
    }
}
