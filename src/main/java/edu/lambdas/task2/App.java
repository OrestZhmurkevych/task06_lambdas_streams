package edu.lambdas.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static PrintStringCommand getCommand(String consoleCommand, String consoleString) {
        PrintStringCommand command;
        StringCombineReceiver referenceReceiver = new StringCombineReceiver(consoleString);
        switch (consoleCommand) {
            case "lambda":
                command = () -> referenceReceiver.buildMessageWithLambdaText();
                break;
            case "reference":
                command = referenceReceiver::buildMessageWithReferenceText;
                break;
            case "anonymous":
                command = new PrintStringCommand() {
                    @Override
                    public String execute() {
                        return "This string was edited by anonymous approach " + consoleString;
                    }
                };
                break;
            case "CommandClass":
                command = new PrintCommandImpl(consoleString);
                break;
            default:
                command = null;
        }
        return command;
    }

    public static void main(String[] args) {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.print("Enter the command(lambda, reference, anonymous or CommandClass) --> ");
            final String consoleCommand = br.readLine();
            System.out.print("\nEnter string for editing --> ");
            String consoleString = br.readLine();

            final PrintStringCommand command = getCommand(consoleCommand, consoleString);
            if (command != null) {
                PrintExecutor executor = new PrintExecutor();
                final String resultOperation = executor.executeOperation(command);
                System.out.print(String.format("Result of operation using %s command: %s", consoleCommand, resultOperation));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
