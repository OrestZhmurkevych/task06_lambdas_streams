package edu.lambdas.task3;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class App {

    public static void main(String[] args) {

        final List<Integer> intListByRange = Generator.getIntListByRange(0, 15, 10);
        System.out.println(intListByRange);
        final int sum = intListByRange.stream()
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println("The sum of elements using stream API(sum): " + sum);
        final double average = intListByRange.stream()
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble();
        System.out.println("The average value of elements using stream API(average): " + average);
        final int min = intListByRange.stream().mapToInt(Integer::intValue).min().getAsInt();
        System.out.println("The minimum value of elements using stream API(min): " + min);
        final int max = intListByRange.stream().mapToInt(Integer::intValue).max().getAsInt();
        System.out.println("The maximum value of elements using stream API(max): " + max);
        final Integer sumByReduce = intListByRange.stream()
                .reduce((x, y) -> x + y)
                .get();
        System.out.println(String.format("The sum of elements using stream API(reduce): %d", sumByReduce));
        final int count = Long.valueOf(intListByRange.stream()
                .filter((intValue) -> intValue > average)
                .count()).intValue();
        System.out.println(String.format("The number of elements that are bigger than average is: %d", count));
    }
}
