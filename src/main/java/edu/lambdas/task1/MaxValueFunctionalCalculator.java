package edu.lambdas.task1;

@FunctionalInterface
public interface MaxValueFunctionalCalculator {

    int calculate(int a, int b, int c);
}
