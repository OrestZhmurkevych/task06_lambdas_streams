package edu.lambdas.task1;

@FunctionalInterface
public interface AverageFunctionalCalculator {
    int calculate(int a, int b, int c);
}
