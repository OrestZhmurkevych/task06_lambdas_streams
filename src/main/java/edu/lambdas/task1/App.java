package edu.lambdas.task1;

public class App {


    public static void main(String[] args) {

        int a = 12;
        int b = 17;
        int c = 8;
        MaxValueFunctionalCalculator maxCalculator = (first, second, third) -> {
            int result = third;
            if (first > second && first > third) {
                result = first;
            } else if (second > first && second > third) {
                result = second;
            }
            return result;
        };
        System.out.print("Max value from three numbers is: " + maxCalculator.calculate(a, b, c));

        AverageFunctionalCalculator averageCalculator = (first, second, third) -> {
            int result = third;
            if ((first > second && first < third) || (first < second && first > third)) {
                result = first;
            } else if ((second > first && second < third) || (second < first && second > third)) {
                result = second;
            }
            return result;
        };
        System.out.print("\nAverage value from three numbers is: " + averageCalculator.calculate(a, b, c));
    }
}



