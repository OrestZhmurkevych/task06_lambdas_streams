package edu.lambdas.task4;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class App {

    private static final String SPACE = " ";


    public static List<String> getText() {
        System.out.print("Enter the text please --> ");
        List<String> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line = StringUtils.EMPTY;
            do {
                line = br.readLine();
                if (StringUtils.isNotEmpty(line)) {
                    result.add(line);
                }
            } while (StringUtils.isNotEmpty(line));
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {

        //User enters the text
        final List<String> text = getText();
        System.out.println(text);
        //Searching of unique words
        final String generalString = text.stream().collect(Collectors.joining(SPACE));
        final String[] splittedWords = generalString.split(SPACE);
        final long numberOfUniqueWords = Arrays.stream(splittedWords).distinct().count();
        System.out.println(String.format("The number of unique words: %d", numberOfUniqueWords));

        //Sorting unique words
        final List<String> listOfSortedWords = Arrays.stream(splittedWords)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(String.format("The sorted list words: %s", listOfSortedWords));

        //Counting of words numbers
        final Map<String, List<String>> wordGroupMap = Arrays.stream(splittedWords).collect(Collectors.groupingBy(String::valueOf));
        final Map<String, Integer> numberOfWordsMap = wordGroupMap.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey(), (entry) -> entry.getValue().size()));
        System.out.println("Occurrences number of each words: " + numberOfWordsMap);

        //Counting numbers of letters
        final String regularString = text.stream()
                .collect(Collectors.joining())
                .replace(SPACE, StringUtils.EMPTY);
        Stream<Character> streamOfCharacters = CharBuffer.wrap(regularString.toCharArray()).chars().mapToObj(ch -> (char)ch);
        final Map<Character, List<Character>> characterGroupMap = streamOfCharacters
                .collect(Collectors.groupingBy(Character::valueOf));
        final Map<Character, Integer> numberOfCharactersMap = characterGroupMap.entrySet().stream()
                .filter(entry -> Character.isLowerCase(entry.getKey()))
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().size()));
        System.out.println(String.format("Occurrences number of each letter: %s", numberOfCharactersMap.toString()));
    }
}
